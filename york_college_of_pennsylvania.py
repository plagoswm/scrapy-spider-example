import scrapy
import re
from ..format.course_terms import get_date


class YorkCollegePennsylvaniaSpider(scrapy.Spider):
    name = 'york_college_of_pennsylvania__3083506'
    allowed_domains = ['bookstore.ycp.edu']
    start_urls = ['https://bookstore.ycp.edu/college']
    custom_settings = {
        "USER_AGENT": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36",
        "ROBOTSTXT_OBEY": False,
        "LOG_FILE": f"{name}.log",
    }
    headers = {
        "User-Agent": custom_settings['USER_AGENT'],
        "Accept": "application/json, text/javascript, */*; q=0.01",
        "Accept-Language": "en-US",
        "X-Requested-With": "XMLHttpRequest",
    }

    main_url = 'https://bookstore.ycp.edu'

    def parse(self, response):
        self.logger.info("{}::{}".format(response.status, response.url))

        for term in response.xpath('//div[@class="container"]/div[contains(.,"Academic Term")]/following-sibling::div/div/a'):
            term_link = term.xpath('@url').get() # ['/college_term/65600', '/college_term/65599', '/college_term/65601']
            term_name = term.xpath('text()').get()
            if get_date(term_name.strip(), self.settings['TERMS_YEAR']):
                url = f'{self.main_url}/timber/college/ajax?l=' + term_link
                yield scrapy.Request(url,
                                     callback=self.get_session,
                                     cb_kwargs=
                                     {
                                         'term_name': term_name,
                                         'term_url': url,
                                     },
                                     headers = self.headers,
                                     dont_filter=True,
                                     )

    def get_session(self, response, term_name, term_url):
        self.logger.info("{}::{}".format(response.status, response.url))

        for i, dep in enumerate(response.xpath('//div[@class="tcc-row"]/div/a')):
            department_url = dep.xpath('@url').get()
            abbrev = dep.xpath('span[@class="abbreviation"]/text()').get()
            #dep_name = dep.xpath('span[@class="name"]/text()').get()
            # ['/college_dept/65642']
            # ['WLD']
            # ['WLD  Welding']

            department_url = f'{self.main_url}/timber/college/ajax?l=' + department_url
            yield scrapy.Request(term_url,
                                 callback=self.get_department,
                                 cb_kwargs={
                                     'term_name': term_name,
                                     'dep_abbrev': abbrev,
                                     'department_url': department_url,
                                 },
                                 meta={'cookiejar': i},
                                 dont_filter=True,

                                 )

    def get_department(self, response, term_name, dep_abbrev, department_url):
        yield scrapy.Request(department_url,
                             dont_filter=True,
                             callback=self.get_courses,
                             meta={'cookiejar': response.meta['cookiejar']},

                             cb_kwargs={
                                 'term_name': term_name,
                                 'dep_abbrev': dep_abbrev,
                             },
                             )

    def get_courses(self, response, term_name, dep_abbrev):
        self.logger.info("{}::{}".format(response.status, response.url))

        for course in response.xpath('//div[@id="tcc-college_course"]/div/div[@class="tcc-row"]/div/a'):
            course_url = course.xpath('@url').get()  # ['/college_course/65622']
            course_info = course.xpath('text()').get() # ['111 - College Student Success ']

            url = f'{self.main_url}/timber/college/ajax?l=' + course_url
            yield scrapy.Request(url,
                                 callback=self.get_sections,
                                 meta={'cookiejar': response.meta['cookiejar']},
                                 cb_kwargs={
                                     'term_name': term_name,
                                      'dep_abbrev': dep_abbrev,
                                     'course_info': course_info
                                 })

    def get_sections(self,response, term_name, dep_abbrev, course_info):
        self.logger.info("{}::{}".format(response.status, response.url))
        for section in response.xpath('//div[@id="tcc-college_section"]/div/div[@class="tcc-row"]/div/a'):
            section_url = section.xpath('@url').get()
            section_info = section.xpath('text()').get()
            if section_url:
                url = f'{self.main_url}/timber/college/ajax?l=' + section_url
                yield scrapy.Request(url,
                                     callback=self.get_requireds,
                                     meta={'cookiejar': response.meta['cookiejar']},

                                     cb_kwargs={
                                         'term_name': term_name,
                                         'dep_abbrev': dep_abbrev,
                                         'course_info': course_info,
                                         'section_info': section_info,
                                     })

    def get_requireds(self,response, term_name, dep_abbrev, course_info, section_info):
        self.logger.info("{}::{}".format(response.status, response.url))

        # course_info , ['111 - College Student Success ']
        course_id = course_info.split('-')[0].strip()
        course_name = course_info.split('-')[1].strip()
        section_instructor = ' '.join(re.findall(r'[A-z]+', section_info)).strip()

        for required in response.xpath('//div[contains(@class, "req-group")]'):
            required_status = required.xpath('div[@class="title"]/text()').get()
            if not required_status:
                book_required = ''
            elif 'prepaid' in required_status.lower():
                book_required = 'Prepaid'
            elif 'optional' in required_status.lower():
                book_required = 'Optional'
            elif 'required' in required_status.lower():
                book_required = 'Required'
            elif 'choose' in required_status.lower():
                book_required = 'Choose'
            elif 'suggested' in required_status.lower():
                book_required = 'Suggested'
            else:
                book_required = ''
                self.logger.info("Unknown textbook status: {} - plz add elif block in current script".format(required_status.lower()))

            for book in required.xpath('div[contains(@class, "timber-item-group")]'):
                isbn = book.xpath('span/text()').get()
                isbn = re.findall(r'\w+', isbn)[0]
                if isbn:
                    title = book.xpath('.//span[@class="tcc-product-title"]/text()').get().strip()
                    edition = ' '.join(book.xpath('text()').getall()).replace('[', '').replace(']', '').strip()
                    author = book.xpath('em[@class="author-data"]/text()').get()
                    yield {
                        'campus_name': "York College Of Pennsylvania",
                        'term_name': term_name,
                        'dep_name': dep_abbrev,
                        'course_number': course_id,
                        'course_name': course_name,
                        'section_instructor': section_instructor,
                        'isbn': isbn,
                        'required': book_required,
                        'title': title + ' ' + edition,
                        'author': author
                    }