import scrapy
from w3lib.html import remove_tags
from urllib.parse import urlencode

from ..format.in_tendhost import extract_time, get_start_window, get_end_window, get_lots
from ..format.in_tendhost_helper import get_tender_values, get_lots
from ..formatting_methods import telephone_check, email_check


class ReadingbcInTendhostOldSpider(scrapy.Spider):
    name = 'in-tendhost.co.uk.readingbc'
    allowed_domains = ['in-tendhost.co.uk']
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36',
        'Accept': 'text/plain, */*',
        'Accept-Language': 'en',
        'Content-Type': 'application/json; charset=utf-8',
        'X-Requested-With': 'XMLHttpRequest',
    }
    tender_code = 'readingbc'
    #tender_code = 'sesharedservices'

    def start_requests(self):
        url = f'https://in-tendhost.co.uk/{self.tender_code}/aspx/Tenders/current'
        yield scrapy.Request(url,
                             callback=self.parse,
                             headers=self.headers,
                             cb_kwargs={'ipage': 1}
                             )

    def parse(self, response, ipage):

        params = {
            "strMode": "current",
            "searchvalue": '',
            "bUseSearch": "false",
            "OrderBy": "Title",
            "OrderDirection": "ASC",
            "iPage": ipage,
            "iPageSize": "10",
            "bOnlyWithCorrespondenceAllowed": "false",
            "iCustomerFilter": '0',
            "iOptInStatus": "-1",
        }

        url = 'https://in-tendhost.co.uk/{tender_code}/aspx/Services/Projects.svc/GetProjects?{params}'.format(
            tender_code = self.tender_code,
            params=urlencode(params)
        )

        yield scrapy.Request(url,
                             callback=self.get_page_items,
                             cb_kwargs={'data': params},
                             headers=self.headers,
                             )

    def get_page_items(self, response, data):
        page_data = response.json()
        current_ipage = int(data['iPage'])
        if page_data['PageCount'] > current_ipage:
            current_ipage += 1
            data['iPage'] = current_ipage
            url = 'https://in-tendhost.co.uk/{tender_code}/aspx/Services/Projects.svc/GetProjects?{params}'.format(
                tender_code = self.tender_code,
                params=urlencode(data)
            )
            yield scrapy.Request(url,
                                 callback=self.parse,
                                 cb_kwargs={'ipage': current_ipage},
                                 headers=self.headers,
                                 dont_filter=True,
                                 )

        for i in page_data['Data']:
            base_tender_url = f'https://in-tendhost.co.uk/{self.tender_code}/aspx/ProjectManage/'

            data = {
                'tender_id': str(i.get('UniqueID')),
                'tender_url': base_tender_url + str(i['ProjectID']),
                'tender_title': i.get('Title'),
                'tender_description': remove_tags(i.get('Description').replace("\t", "")),

                'tender_categories': [
                    {'code': cpv[:8], 'description': None} for cpv in i.get('CPV').split()
                ],

                'tender_upper_value': None,
                'tender_lower_value': None,
                'tender_value': None,
                "tender_value_currency": 'GBR',

                'tender_created': extract_time(i.get('EstStartDate')),
                'tender_start_date': extract_time(i.get('OpenDate')),
                'tender_end_date': extract_time(i.get('CloseDate')),

                'tender_start_of_interest_window': get_start_window(response),
                'tender_end_of_interest_window': get_end_window(response),

                'tender_buyer': i.get('Customer'),
                'tender_contact': i.get('Contact'),
                'tender_source': self.name
            }

            if i.get('EstValue'):
                for values in get_tender_values(i.get('EstValue')):
                    data.update(values)

            project_id = i.get('ProjectID')

            find_tender_url = f'https://in-tendhost.co.uk/{self.tender_code}/aspx/ViewDocument.aspx?dt=20&id={project_id}&dl=1'
            yield scrapy.Request(find_tender_url,
                                 callback=self.get_find_tender_info,
                                 cb_kwargs={'data': data,
                                            'raw_est_value': i.get('EstValue')
                                            },
                                 )

    def get_find_tender_info(self, response, data, raw_est_value):

        data['tender_keywords'] = None
        data['tender_supply_locations'] = None
        data['tender_attachments'] = None
        data['tender_links'] = None

        if response.xpath('//div[@id="header"]'):
            categories = [response.xpath('//span[contains(.,"Main CPV code")]/following::ul/li/text()').get()]
            data['tender_categories'] = [
                {'code': category.split('-')[0].strip(), 'description': category.split('-')[1]} for category in categories
            ]

            telephone = response.xpath("//strong[.='Telephone']/following::p[1]/text()").get()
            data['tender_telephone'] = telephone_check(telephone)

            email = response.xpath('//a[starts-with(@href, "mailto")]/text()').get()
            data['tender_email'] = email_check(email)

            address = response.xpath('//span[contains(.,"Name and addresses")]/../../following-sibling::p[not (preceding-sibling::h6)]/text()').getall()
            data['tender_address'] = ', '.join(address)

            data['nuts_codes'] = response.xpath('//strong[contains(.,"NUTS code")]/following::p[1]/text()').get()
            data['lots'] = get_lots(response, data, raw_est_value)
        else:
            data['tender_telephone'] = None
            data['tender_email'] = None
            data['tender_address'] = None
            data['nuts_codes'] = None
            data['lots'] = None

        yield data
